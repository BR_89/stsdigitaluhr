package de.stellwerksim.br_89.plugins.digitalclock;

import de.stellwerksim.br_89.plugins.digitalclock.clocks.Clock;
import de.stellwerksim.br_89.plugins.digitalclock.clocks.SimtimeClock;
import de.stellwerksim.br_89.plugins.digitalclock.clocks.SimtimeClockConnection;
import de.stellwerksim.br_89.plugins.digitalclock.gui.ClockPanel;
import de.stellwerksim.br_89.plugins.digitalclock.gui.HHMMSSClockPanel;
import java.io.IOException;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public class Main extends JFrame {

    public final static String AUTHOR = "BR 89";
    public final static String NAME = "Simtime Digitaluhr";
    public final static String TEXT = "Große Digitaluhr";
    public static String VERSION = "0.0.0";
    public static UUID UUID;

    public Main() {
        Properties props = new Properties();
        try {
            props.load(Main.class.getResourceAsStream("/de/stellwerksim/br_89/plugins/digitalclock/Main.properties"));
            VERSION = props.getProperty("version", null);
            UUID = UUID.fromString(props.getProperty("uuid", null));
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setType(java.awt.Window.Type.UTILITY);
        Clock c = new SimtimeClock(new SimtimeClockConnection());
        ClockPanel cp = new HHMMSSClockPanel(c);
        this.add(cp);
        this.pack();
        this.setResizable(false);
        this.setTitle(NAME + " - " + VERSION + " - " + AUTHOR);
        this.setVisible(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(() -> {
            new Main();
        });
    }

    @Override
    public String getName() {
        return NAME;
    }
}
