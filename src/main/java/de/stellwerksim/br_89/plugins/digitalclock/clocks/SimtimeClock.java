package de.stellwerksim.br_89.plugins.digitalclock.clocks;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public class SimtimeClock extends HHMMSSClock {
    protected ISimClockConnection stcc;

    public SimtimeClock(ISimClockConnection stcc, String host) {
        super(4, 59, 59);
        this.stcc = stcc;
        this.stcc.setSimtimeClock(this);
        try {
            stcc.connect(host);
        } catch (IOException ex) {
            Logger.getLogger(SimtimeClock.class.getName()).log(Level.SEVERE, null, ex);
            stcc.closed();
        }
    }
    
    public SimtimeClock(SimtimeClockConnection stcc) {
        this(stcc, "localhost");
    }
    
    @Override
    public void increment() {
        if (secondsCounter.increment()) {
            stcc.updateTime();
        }
    }
    
    @Override
    public void update() {
        this.increment();
        if (this.panel != null) {
            panel.repaint();
        }
    }
}
