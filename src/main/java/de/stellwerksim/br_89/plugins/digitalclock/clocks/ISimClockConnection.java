package de.stellwerksim.br_89.plugins.digitalclock.clocks;

import java.io.IOException;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public interface ISimClockConnection {

    void setSimtimeClock(SimtimeClock stc);

    void updateTime();

    void connect(String host) throws IOException;

    void closed();
}
