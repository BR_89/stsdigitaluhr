package de.stellwerksim.br_89.plugins.digitalclock.clocks;

import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import js.java.stspluginlib.PluginClient;
import de.stellwerksim.br_89.plugins.digitalclock.Main;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public class SimtimeClockConnection extends PluginClient implements ISimClockConnection {
    protected SimtimeClock stc;
    
    public SimtimeClockConnection() {
        super(Main.NAME, Main.AUTHOR, Main.VERSION, Main.TEXT);
        Calendar c = Calendar.getInstance();
        c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE), 0, 0);
    }
    
    @Override
    public void setSimtimeClock(SimtimeClock stc) {
        this.stc = stc;
    }

    @Override
    protected void connected() {
        long time = this.getSimutime();
        time = time % 1000;
        stc.start(time);
    }

    @Override
    public void closed() {
        System.exit(1);
    }
    
    @Override
    public void updateTime() {
        long simtime = this.getSimutime();
        stc.secondsCounter.setValue((int)TimeUnit.MILLISECONDS.toSeconds(simtime)%60);
        stc.minutesCounter.setValue((int)TimeUnit.MILLISECONDS.toMinutes(simtime)%60);
        stc.hoursCounter.setValue((int)TimeUnit.MILLISECONDS.toHours(simtime));
    }

    @Override
    protected void response_anlageninfo(int aid, String name, String build) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void response_bahnsteigliste(HashMap<String, HashSet<String>> bl) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void response_zugliste(HashMap<Integer, String> zl) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void response_zugdetails(int zid, PluginClient.ZugDetails details) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void response_zugfahrplan(int zid, LinkedList<PluginClient.ZugFahrplanZeile> plan) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
