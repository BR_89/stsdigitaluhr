package de.stellwerksim.br_89.plugins.digitalclock.gui;

import java.awt.Dimension;
import java.awt.Graphics;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public class SevenSegmentDelimiter extends SevenSegmentComponent {
    private boolean showDots = false;

    public void setShowDots(boolean showDots) {
        this.showDots = showDots;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        setColor(g, showDots);
        g.fillRect(10, 45, 10, 10);
        g.fillRect(10, 115, 10, 10);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(30, 170);
    }
}
