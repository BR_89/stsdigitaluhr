/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.stellwerksim.br_89.plugins.digitalclock.gui;

import de.stellwerksim.br_89.plugins.digitalclock.clocks.Clock;
import de.stellwerksim.br_89.plugins.digitalclock.clocks.HHMMSSClock;
import de.stellwerksim.br_89.plugins.digitalclock.counter.DoubleDigitCounter;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public class HHMMSSClockPanel extends ClockPanel {

    protected final SevenSegmentDisplayBlock displayBlockSeconds = new SevenSegmentDisplayBlock();
    protected final SevenSegmentDisplayBlock displayBlockMinutes = new SevenSegmentDisplayBlock();
    protected final SevenSegmentDisplayBlock displayBlockHours = new SevenSegmentDisplayBlock();
    protected final SevenSegmentDelimiter displayDelimiterMinutesSeconds = new SevenSegmentDelimiter();
    protected final SevenSegmentDelimiter displayDelimiterHoursMinutes = new SevenSegmentDelimiter();

    public HHMMSSClockPanel(Clock clock) {
        super(clock);
        this.add(displayBlockHours);
        this.add(displayDelimiterHoursMinutes);
        this.add(displayBlockMinutes);
        this.add(displayDelimiterMinutesSeconds);
        this.add(displayBlockSeconds);
    }

    @Override
    public void repaint() {
        HHMMSSClock c = (HHMMSSClock) clock;
        if (c != null) {
            if (c.getSecondsCounter() != null) {
                DoubleDigitCounter secondsCounter = c.getSecondsCounter();
                displayBlockSeconds.setValue(secondsCounter.getValue());
                displayDelimiterHoursMinutes.setShowDots(secondsCounter.getLowerValue() % 2 == 0);
                displayDelimiterMinutesSeconds.setShowDots(secondsCounter.getLowerValue() % 2 == 0);
            }
            if (c.getMinutesCounter() != null) {
                displayBlockMinutes.setValue(c.getMinutesCounter().getValue());
            }
            if (c.getHoursCounter() != null) {
                displayBlockHours.setValue(c.getHoursCounter().getValue());
            }
        }
        super.repaint();
    }
}
