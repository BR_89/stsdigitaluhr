package de.stellwerksim.br_89.plugins.digitalclock.gui;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public class SevenSegmentDisplayBlock extends javax.swing.JPanel {

    /**
     * Creates new form SevenSegmentDisplayBlock
     */
    public SevenSegmentDisplayBlock() {
        initComponents();
    }
    
    public int getLowerValue() {
        return lower.getValue();
    }
    
    public void setLowerValue(int value) {
        lower.setValue(value);
    }

    public int getUpperValue() {
        return upper.getValue();
    }
    
    public void setUpperValue(int value) {
        upper.setValue(value);
    }
    
    public void setValue(int value) {
        lower.setValue(value % 10);
        upper.setValue(value / 10);
    }
    
    public void setAlarm(boolean alarm) {
        lower.setAlarm(alarm);
        upper.setAlarm(alarm);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        upper = new de.stellwerksim.br_89.plugins.digitalclock.gui.SevenSegmentDisplay();
        lower = new de.stellwerksim.br_89.plugins.digitalclock.gui.SevenSegmentDisplay();

        javax.swing.GroupLayout upperLayout = new javax.swing.GroupLayout(upper);
        upper.setLayout(upperLayout);
        upperLayout.setHorizontalGroup(
            upperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        upperLayout.setVerticalGroup(
            upperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 170, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout lowerLayout = new javax.swing.GroupLayout(lower);
        lower.setLayout(lowerLayout);
        lowerLayout.setHorizontalGroup(
            lowerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        lowerLayout.setVerticalGroup(
            lowerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 170, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(upper, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lower, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(upper, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(lower, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private de.stellwerksim.br_89.plugins.digitalclock.gui.SevenSegmentDisplay lower;
    private de.stellwerksim.br_89.plugins.digitalclock.gui.SevenSegmentDisplay upper;
    // End of variables declaration//GEN-END:variables
}
