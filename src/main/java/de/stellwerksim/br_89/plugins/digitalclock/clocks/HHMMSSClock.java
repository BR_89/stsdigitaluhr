/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.stellwerksim.br_89.plugins.digitalclock.clocks;

import de.stellwerksim.br_89.plugins.digitalclock.counter.DoubleDigitCounter;
import de.stellwerksim.br_89.plugins.digitalclock.counter.HourDoubleDigitCounter;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public class HHMMSSClock extends Clock {
    protected final DoubleDigitCounter secondsCounter = new DoubleDigitCounter(5, 9);
    protected final DoubleDigitCounter minutesCounter = new DoubleDigitCounter(5, 9);
    protected final DoubleDigitCounter hoursCounter = new HourDoubleDigitCounter();

    public HHMMSSClock() {
        super();
    }

    public HHMMSSClock(int hours, int minutes, int seconds) {
        this();
        secondsCounter.setValue(seconds);
        minutesCounter.setValue(minutes);
        hoursCounter.setValue(hours);
    }

    public void increment() {
        if (secondsCounter.increment()) {
            if (minutesCounter.increment()) {
                hoursCounter.increment();
            }
        }
    }

    public void decrement() {
        if (secondsCounter.decrement()) {
            if (minutesCounter.decrement()) {
                hoursCounter.decrement();
            }
        }
    }

    @Override
    public void update() {
    }

    public DoubleDigitCounter getHoursCounter() {
        return hoursCounter;
    }

    public DoubleDigitCounter getMinutesCounter() {
        return minutesCounter;
    }

    public DoubleDigitCounter getSecondsCounter() {
        return secondsCounter;
    }
}
