package de.stellwerksim.br_89.plugins.digitalclock.counter;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public class DoubleDigitCounter {

    protected SingleDigitCounter lower;
    protected SingleDigitCounter upper;

    public DoubleDigitCounter(int upperBound, int lowerBound) {
        upper = new SingleDigitCounter(upperBound);
        lower = new SingleDigitCounter(lowerBound);
    }

    public boolean increment() {
        if (lower.increment()) {
            return upper.increment();
        }
        return false;
    }

    public boolean decrement() {
        if (lower.decrement()) {
            return upper.decrement();
        }
        return false;
    }

    public void setValue(int value) {
        lower.setValue(value % 10);
        upper.setValue(value / 10);
    }

    public int getValue() {
        return upper.getValue() * 10 + lower.getValue();
    }

    public int getLowerValue() {
        return lower.getValue();
    }

    public int getUpperValue() {
        return upper.getValue();
    }

    @Override
    public String toString() {
        return Integer.toString(upper.getValue()) + Integer.toString(lower.getValue());
    }
}
