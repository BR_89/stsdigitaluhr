package de.stellwerksim.br_89.plugins.digitalclock.counter;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public class SingleDigitCounter {

    private int value = 0;
    private final int overflow;

    public SingleDigitCounter(int overflow) {
        this.overflow = overflow;
    }

    public boolean increment() {
        if (value >= overflow) {
            value = 0;
            return true;
        }
        value++;
        return false;
    }

    public boolean decrement() {
        if (value <= 0) {
            value = overflow;
            return true;
        }
        value--;
        return false;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return Integer.toString(getValue());
    }
}
