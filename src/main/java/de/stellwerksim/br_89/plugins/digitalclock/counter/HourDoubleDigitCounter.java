package de.stellwerksim.br_89.plugins.digitalclock.counter;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public class HourDoubleDigitCounter extends DoubleDigitCounter {

    public HourDoubleDigitCounter() {
        super(2, 9);
    }

    @Override
    public boolean decrement() {
        if (super.decrement())
        {
            upper.setValue(2);
            lower.setValue(3);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean increment() {
        if (lower.increment())
        {
            return upper.increment();
        }
        else
        {
            if (upper.getValue() == 2 && lower.getValue() >= 4)
            {
                upper.setValue(0);
                lower.setValue(0);
                return true;
            }
        }
        return false;
    }
    
}
