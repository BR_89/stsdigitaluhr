package de.stellwerksim.br_89.plugins.digitalclock.clocks;

import de.stellwerksim.br_89.plugins.digitalclock.gui.ClockPanel;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public abstract class Clock {

    protected ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
    protected boolean alarm;
    protected ClockPanel panel;

    public Clock() {
    }

    public void start() {
        this.executor.scheduleWithFixedDelay(() -> {
            update();
        }, 1, 1, TimeUnit.SECONDS);
    }

    public void start(long wait) {
        this.executor.scheduleWithFixedDelay(() -> {
            update();
        }, wait, 1000, TimeUnit.MILLISECONDS);
    }

    public abstract void update();

    public void stop() {
        this.executor.shutdown();
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    public boolean getAlarm() {
        return this.alarm;
    }

    public void setPanel(ClockPanel panel) {
        this.panel = panel;
    }

    public ClockPanel getPanel() {
        return panel;
    }
}
