package de.stellwerksim.br_89.plugins.digitalclock.gui;

import java.awt.Dimension;
import java.awt.Graphics;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public class SevenSegmentDisplay extends SevenSegmentComponent {

    private TimeDigitCode value;

    public SevenSegmentDisplay() {
        this.value = TimeDigitCode.values()[10];
    }

    public synchronized void setValue(int value) {
        try {
            this.value = TimeDigitCode.values()[value];
        } catch (ArrayIndexOutOfBoundsException e) {
            this.value = TimeDigitCode.values()[10];
        }
        this.repaint();
    }

    public synchronized int getValue() {
        return value.ordinal();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        boolean[] values = value.getValue();

        setColor(g, values[0]);
        g.fillRect(20, 10, 60, 10);

        setColor(g, values[1]);
        g.fillRect(80, 20, 10, 60);

        setColor(g, values[2]);
        g.fillRect(80, 90, 10, 60);

        setColor(g, values[3]);
        g.fillRect(20, 150, 60, 10);

        setColor(g, values[4]);
        g.fillRect(10, 90, 10, 60);

        setColor(g, values[5]);
        g.fillRect(10, 20, 10, 60);

        setColor(g, values[6]);
        g.fillRect(20, 80, 60, 10);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(100, 170);
    }
}

enum TimeDigitCode {

    ZERO(new boolean[]{true, true, true, true, true, true, false}),
    ONE(new boolean[]{false, true, true, false, false, false, false}),
    TWO(new boolean[]{true, true, false, true, true, false, true}),
    THREE(new boolean[]{true, true, true, true, false, false, true}),
    FOUR(new boolean[]{false, true, true, false, false, true, true}),
    FIVE(new boolean[]{true, false, true, true, false, true, true}),
    SIX(new boolean[]{true, false, true, true, true, true, true}),
    SEVEN(new boolean[]{true, true, true, false, false, false, false}),
    EIGHT(new boolean[]{true, true, true, true, true, true, true}),
    NINE(new boolean[]{true, true, true, true, false, true, true}),
    OFF(new boolean[]{false, false, false, false, false, false, false});
    private boolean[] value;

    private TimeDigitCode(boolean[] value) {
        this.value = value;
    }

    public boolean[] getValue() {
        return this.value;
    }
}