package de.stellwerksim.br_89.plugins.digitalclock.gui;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
abstract public class SevenSegmentComponent extends javax.swing.JComponent {

    protected boolean isAlarm = false;
    public static Color ALARM = Color.RED;
    public static Color ON = Color.BLACK;
    public static Color OFF = Color.LIGHT_GRAY;

    protected void setColor(Graphics g, boolean b) {
        if (b) {
            if (isAlarm) {
                g.setColor(ALARM);
                return;
            }
            g.setColor(ON);
            return;
        }
        g.setColor(OFF);
    }

    public void setAlarm(boolean alarm) {
        isAlarm = alarm;
    }
}
