package de.stellwerksim.br_89.plugins.digitalclock.gui;

import javax.swing.JPanel;
import de.stellwerksim.br_89.plugins.digitalclock.clocks.Clock;

/**
 *
 * @author Axel BR 89 Lehmann <axel@stellwerksim.de>
 */
public abstract class ClockPanel extends JPanel {

    protected final Clock clock;
    
    public ClockPanel(Clock clock) {
        this.clock = clock;
        this.clock.setPanel(this);
    }
}
